package innometrics.agents.aggregator.service;

import innometrics.agents.aggregator.model.Metric;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class AggregatorService {


    @Autowired
    MetricRepository metricRepository;

    public void saveMetric(List<Metric> metrics){
        metricRepository.saveAll(metrics);
    }

//    public List getMetric(MetricType type){
//
//        MongoRepository repository = mapper.getRepositoryForType(type);
//
//        return repository.findAll();
//    }






}
