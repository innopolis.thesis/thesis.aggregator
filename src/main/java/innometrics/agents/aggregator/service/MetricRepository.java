package innometrics.agents.aggregator.service;

import innometrics.agents.aggregator.model.Metric;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MetricRepository extends JpaRepository<Metric,Long> {
}
