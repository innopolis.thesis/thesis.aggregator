package innometrics.agents.aggregator.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table
@Data
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Metric {

    @Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
    Long uid;

    @Column
    String metricId;

    @Column
    String author;

    @Column
    Long projectId;


    @Column
    Long agentId;

    @Column
    Long metricType;

    @Column
    String meta;

    @Column
    Timestamp time;




}
