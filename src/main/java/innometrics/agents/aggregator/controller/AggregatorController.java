package innometrics.agents.aggregator.controller;

import com.fasterxml.jackson.databind.JsonNode;
import innometrics.agents.aggregator.model.Metric;
import innometrics.agents.aggregator.service.AggregatorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AggregatorController {

    @Autowired
    AggregatorService service;

    @PostMapping("metric")
    public void saveMetrics(@RequestBody List<Metric> metrics){

        log.info("accept metrics, {}",metrics);
        service.saveMetric(metrics);
        log.info("finished");
    }

//    @GetMapping("metric") List getMetrics(@RequestParam(name = "type") MetricType type){
//        log.info("request to get metric {}",type);
//
//        List metric = service.getMetric(type);
//
//        log.info("return metrics {}",metric);
//
//        return metric;
//    }
}
